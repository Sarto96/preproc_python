import random 

#random Alice sequence 
def aSeq(x):
    s=[]
    for _ in range(x):
        s.append(random.randint(0,1))
    return s

#Periodic Alice sequence
def aSeq():
    s=list()
    for _ in range(6_250_000 * 4):
        s.append(0)
        s.append(1)
    return s

#Periodic mu intensity sequence
def muSeq():
    mu=list()
    for _ in range(6_250_000):    
        for _ in range(4):
            mu.append("1 1 1 1".split(" "))
        for _ in range(4):
            mu.append("2 2 2 2".split(" "))
    return mu


eta=0.7                 #transmittance channel + receiver error
mu_1=1
mu_2=1
mu=[mu_1, mu_2]         #in practice mu=[m1,m2]
T=50#*10^6              #period
Jitter=2                #nanosecond
Qz=Qx=0.05              #QBER     
Pz=0.5;Px=1-Pz          #Bob base decision


sA=[0, 1, 0, 0, 0, 1, 1, 0, 1, 0]
#sA=aSeq()
sB=list()               #(0,1)->OK (-1)->lost
time_tag=list()
channel_tag=list()
for i, x in enumerate(sA, 1):
    #select mu
    det_rate = eta*mu[0] if random.random()>0.5 else eta*mu[1]
    #lost
    if random.random()>det_rate:
        sB.append(-1)
        continue
    #received
    else:
        J=round(random.gauss(0,Jitter),2)
        #H-V Base
        if random.random()<Pz:
            #correct channel
            if random.random()<Qz:
                t=i*T+J
                time_tag.append(t)
                channel_tag.append(1 if x==0 else 2)
                sB.append(x)
                #wrong channel
            else:
                t=i*T+J
                time_tag.append(t)
                channel_tag.append(2 if x==0 else 1)
                sB.append(1 if x==0 else 0)
        #D-A Base
        else:
            if random.random()<Qx:
                t=i*T+J
                time_tag.append(t)
                channel_tag.append(3 if x==0 else 4)
                sB.append(x)
            #received on the wrong channel
            else:
                t=i*T+J
                time_tag.append(t)
                channel_tag.append(4 if x==0 else 3)
                sB.append(1 if x==0 else 0)
                
time_tag.extend(channel_tag)
# print(sA)
# print(sB)
#print(time_tag)